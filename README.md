# Installation de l'environnement back-end d'ATM Store

Prérequis: Docker est installé (Docker toolbox ou Docker Desktop)

## Construire les containers

```Bash
docker-compose up --build -d
```

> Si le message suivant s'affiche:
> **Couldn't connect to Docker daemon - you might need to run `docker-machine start default`.**
> Executez alors le terminal en tant qu'administrateur

Lorsque les containers ont fini d'être build alors on souhaite récupérer l'ip de la machine virtuel pour y accéder.

```Bash
docker-machine ip
```

Récupérer l'adresse ip donné et avec elle vous pourrez accèder aux différents containers par le navigateur.

* MySQL: 3306
* django/nginx: 80
* adminer: 8080

> Si vous utilisez docker toolbox alors dans le fichier `settings.py` alors modifiez ALLOWED_HOST pour y avoir '192.168.99.100' et HOST': '192.168.99.100'

> Si vous utilisez docker desktop et docker sur linux alors dans le fichier `settings.py` alors modifiez ALLOWED_HOST pour y avoir 'localhost' et HOST': 'localhost'

## MySQL

La base de données utilisées pour fournir les informations d'un profil est Apache MySQL 8.0.17.

* nom de la base: 'configurations'
* utilisateur: 'root'
* mot de passe: 'root'

## adminer

Adminer est un logiciel très léger fournissant une interface graphique pour gérer la base de données. Il fournit une vision des tables de la base ainsi que d'un terminal pour réaliser des actions sur les tables.

## Django

Les informations concernants la base de données accédée par django est définie dans le fichier `settings.py` dans l'objet DATABASES.

> Pour l'environnement de développement `ALLOWED_HOSTS` accepte toutes les adresses IP ce qui n'estp as un problème pour le dev mais lors du déploiement celà peut devenir un problème si le serveur propose plusieurs nom de domaine car django répondra aux endpoints d'URL sur tous les domaines de la machine.

### Endpoints

`http://192.168.99.100/api/profile/` permet de récupérer la liste des profiles stockés dans la base de données.

`http://192.168.99.100/api/application/` permet de récupérer les informations des applications pouvant être ajoutées dans un profil.

`http://192.168.99.100/api/parameters/` Renvoie une liste de paramètres pour chaque liens entre applications et profiles.

`http://192.168.99.100/api/profile_applications/` liste des associations entre profiles et applications, donne la liste des applications pour chaque profils.

Liens accessibles :
`http://localhost:8000/atm/<airport>/<role>/` permet de récupérer pour un aéroport donné et un role donné les applications et leurs configurations associées.
