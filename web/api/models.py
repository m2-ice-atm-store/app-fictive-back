from django.db import models

# Create your models here.

class Profile(models.Model):

    """
    Profile Model
    Contains list of profiles
    """

    Id = models.AutoField(primary_key=True)
    Role = models.CharField(max_length=50)
    Airport = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'Profile'


class Application(models.Model):

    """
    Application Model
    Contains list of applications
    """

    Id = models.AutoField(primary_key=True)
    Name = models.CharField(max_length=100)
    Version = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'Application'


class Parameter(models.Model):

    """
    Parameter Model
    Contains list of parameters
    """

    Id = models.AutoField(primary_key=True)
    Key = models.CharField(max_length=100)
    Value = models.CharField(max_length=100)
    IdProfile = models.ForeignKey(Profile, models.DO_NOTHING, db_column='profile.id', related_name='parameters')
    IdApplication = models.ForeignKey(Application, models.DO_NOTHING, db_column='application.id')

    class Meta:
        managed = False
        db_table = 'Parameter'


class Profile_Applications(models.Model):

    """
    Profile_Applications Model
    Contain list of profile_application
    """

    Id = models.AutoField(primary_key=True)
    IdProfile = models.ForeignKey(Profile, models.DO_NOTHING, db_column='profile.id', related_name='applications')
    IdApplication = models.ForeignKey(Application, models.DO_NOTHING, db_column='application.id')

    def application(self):
        return {'Id': self.IdApplication.Id, 'Name': self.IdApplication.Name, 'Version': self.IdApplication.Version}

    class Meta:
        managed = False
        db_table = 'Profile_Applications'
