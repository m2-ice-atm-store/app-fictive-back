from django.shortcuts import render
from django.http import Http404
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import *

# Create your views here.

# Default
# Profile
class ProfileAPI(viewsets.ModelViewSet):
    serializer_class = ProfileSerializer
    queryset = Profile.objects.all()      

# Application
class ApplicationAPI(viewsets.ModelViewSet):
    serializer_class = ApplicationSerializer
    queryset = Application.objects.all()


# Parameter
class ParameterAPI(viewsets.ModelViewSet):
    serializer_class = ParameterSerializer
    queryset = Parameter.objects.all()


# Profile_Applications
class Profile_ApplicationsAPI(viewsets.ModelViewSet):
    serializer_class = Profile_ApplicationsSerializer
    queryset = Profile_Applications.objects.all()

# Detail
# Profile detail
class ProfileDetailAPI(viewsets.ModelViewSet):
    serializer_class = ProfileDetailSerializer
    queryset = Profile.objects.all()

    def get_queryset(self):
        airport = self.kwargs['airport']
        role = self.kwargs['role']
        return Profile.objects.filter(Airport=airport, Role=role)