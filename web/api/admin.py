from django.contrib import admin
from .models import Profile
# Register your models here.

class ProfileAdmin(admin.ModelAdmin):
    list_display = ('Role','Airport')
    search_fields = ('Role','Airport')
    list_filter = ('Role','Airport')
    save_on_top = True

admin.site.register(Profile, ProfileAdmin)