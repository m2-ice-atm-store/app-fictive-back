from rest_framework import serializers
from .models import *

# Default
# Profile
class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = "__all__"

# Application
class ApplicationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Application
        fields = "__all__"

# Parameter
class ParameterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Parameter
        fields = "__all__"

# Profile_Applications
class Profile_ApplicationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile_Applications
        fields = "__all__"

# Detail
# Profile_Applications Detail
class Profile_ApplicationsDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile_Applications
        fields = ['Id', 'application']

# Profile Detail
class ProfileDetailSerializer(serializers.ModelSerializer):
    parameters = ParameterSerializer(many=True)
    applications = Profile_ApplicationsDetailSerializer(many=True)
    class Meta:
        model = Profile
        fields = ['Id', 'Role', 'Airport', 'parameters', 'applications']